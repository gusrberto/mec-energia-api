import pytest

from mec_energia import settings

class UsuarioBase:
    def __init__(self):
        username = ""
        password = ""

    def set_password(self, password):
        self.password = password    

valid_passwords = pytest.mark.parametrize('password', [
    ('senha123'),
    ('teste'),
    ('abcd1234'),
])

@valid_passwords        
def test_case_1(password: str):
    settings.ENVIRONMENT = 'development'

    user = UsuarioBase()

    user.set_password(password)

    assert user.password == password

@valid_passwords
def test_case_2(password: str):
    settings.ENVIRONMENT = 'test'

    user = UsuarioBase()

    user.set_password(password)

    assert user.password == password

def test_case_3():
    settings.ENVIRONMENT = 'production'

    assert settings.ENVIRONMENT == 'production'





